# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=664"]

  # this is insecure because we're just leaving it in the repository, but that's fine for vagrant
  # also copy for every vagrant VM so that we can just log in
  config.vm.provision "shell", inline: <<-SHELL
    cat /vagrant/vagrant/id_ed25519.pub >> /home/vagrant/.ssh/authorized_keys
    cp /vagrant/vagrant/ssh_config /etc/ssh/ssh_config
  SHELL

  # the host used to run ansible from, used for testing
  config.vm.define "ansible_host", primary: true do |ansible_host|
    ansible_host.vm.box = "debian/contrib-stretch64"
    ansible_host.vm.hostname = 'ansible-host'
    ansible_host.vm.provision "shell", inline: <<-SHELL
      if [ ! -f /etc/first_run_done ]; then
        export DEBIAN_FRONTEND=noninteractive
        apt-get update
        apt-get -y install dirmngr
        echo deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main >> /etc/apt/sources.list
        apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
        apt-get update
        apt-get -y install ansible rsync
        touch /etc/first_run_done
      fi

      cp /vagrant/vagrant/id_ed25519 /home/vagrant/.ssh/id_ed25519
    SHELL

    ansible_host.vm.network "private_network", ip: "10.3.0.1/24", virtualbox__intnet: 'westtel_ansible_site'
  end

  config.vm.define "common", autostart: false do |common|
    common.vm.box = "debian/contrib-stretch64"
    common.vm.hostname = 'common'
    common.vm.network "private_network", ip: "10.3.0.2/24", virtualbox__intnet: 'westtel_ansible_site'
  end

  config.vm.define "i386", autostart: false do |i386|
    i386.vm.box = "bento/debian-9.5-i386"
    i386.vm.hostname = "i386"
    i386.vm.network "private_network", ip: "10.3.0.3", virtualbox__intnet: 'westtel_ansible_site'
  end
end
