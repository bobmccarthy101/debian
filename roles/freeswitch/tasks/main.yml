---
- name: include OS/Arch variables
  include_vars: "{{ ansible_distribution_release }}-{{ ansible_userspace_architecture }}.yml"
- name: download FreeSWITCH share
  get_url:
    url: "{{ freeswitch_share_uri }}"
    checksum: "{{ freeswitch_share_checksum }}"
    dest: /usr/local/src/freeswitch-share-1.6.18.tbz
    url_username: "{{ lookup('env', 'ARTIFACT_USER') }}"
    url_password: "{{ lookup('env', 'ARTIFACT_PASSWORD') }}"
- name: download FreeSWITCH opt
  get_url:
    url: "{{ freeswitch_opt_uri }}"
    checksum: "{{ freeswitch_opt_checksum }}"
    dest: /usr/local/src/freeswitch-opt-1.6.18.tbz
    url_username: "{{ lookup('env', 'ARTIFACT_USER') }}"
    url_password: "{{ lookup('env', 'ARTIFACT_PASSWORD') }}"
- name: download and install fs_config
  get_url:
    url: http://artifact.westtel.com/westtel/build/fs_config/fs_config-0.1.1
    checksum: "sha256:8c597976c1a53471ea79751c5646441a2f909b6c5f6edf20db054cbce84021a5"
    dest: /usr/local/bin/fs_config
    url_username: "{{ lookup('env', 'ARTIFACT_USER') }}"
    url_password: "{{ lookup('env', 'ARTIFACT_PASSWORD') }}"
    mode: "u=rwx,go=rx"
- name: unpack FreeSWITCH opt
  unarchive:
    remote_src: yes
    src: /usr/local/src/freeswitch-opt-1.6.18.tbz
    dest: /opt/
    creates: /opt/freeswitch
    extra_opts:
      # rename first component to just "freeswitch"
      - '--transform=flags=r;s_^[^/]*\($\|/\)_freeswitch\1_'
# we only do this if the freeswitch share is not already there
# TODO: upgrading share
- name: unpack FreeSWITCH share
  unarchive:
    remote_src: yes
    src: /usr/local/src/freeswitch-share-1.6.18.tbz
    dest: /usr/local/share/
    creates: /usr/local/share/freeswitch
    extra_opts:
      # rename first component to just "freeswitch"
      - '--transform=flags=r;s_^[^/]*\($\|/\)_freeswitch\1_'
- name: install FreeSWITCH prerequisites
  apt:
    name:
      - acl # needed for setfacl
      - iptables-persistent # needed for firewall rules
      - libcurl3 # needed for libcurl4.so
      - libjbig0
      - libjpeg62-turbo
      - libldns2
      - liblua5.2-0
      - libopus0
      - libpq5
      - libsndfile1
      - libspeex1
      - libspeexdsp1
      - lua-socket # needed for script
      - rsync # needed for synchronize
- name: add FreeSWITCH group
  group:
    name: freeswitch
    system: yes
- name: add FreeSWITCH user
  user:
    name: freeswitch
    create_home: no
    system: yes
    group: freeswitch
    home: /nonexistent
- name: freeswitch lib
  file:
    state: directory
    path: /var/lib/freeswitch
    owner: freeswitch
    group: freeswitch
- name: freeswitch log
  file:
    state: directory
    path: /var/log/freeswitch
    owner: freeswitch
    group: freeswitch
- name: allow adm to read logs (default)
  acl:
    state: present
    default: yes
    path: /var/log/freeswitch
    entity: adm
    etype: group
    permissions: rx
    recursive: yes
    recalculate_mask: no_mask
- name: allow adm to read logs
  acl:
    state: present
    path: /var/log/freeswitch
    entity: adm
    etype: group
    permissions: rx
    recursive: yes
    recalculate_mask: no_mask
- name: fs_cli.conf
  template:
    src: fs_cli.conf.j2
    dest: /etc/fs_cli.conf
- name: allow adm to read fs_cli
  acl:
    path: /etc/fs_cli.conf
    entity: adm
    etype: group
    permissions: r
- name: allow freeswitch to read fs_cli
  acl:
    path: /etc/fs_cli.conf
    entity: freeswitch
    etype: user
    permissions: r
- name: add fs_cli to path
  file:
    state: link
    src: /opt/freeswitch/bin/fs_cli
    dest: /usr/local/bin/fs_cli
    force: yes
    follow: no
- name: install freeswitch.service
  template:
    src: freeswitch.service.j2
    dest: /etc/systemd/system/freeswitch.service
  notify: systemd daemon-reload
# TODO: how to do the actual upgrade
- name: stat /etc/freeswitch
  stat:
    path: /etc/freeswitch/
  register: st_freeswitch
- name: install basic freeswitch config
  synchronize:
    src: freeswitch/
    dest: /etc/freeswitch/
    owner: no # we want root to own etc
    group: no
  when: not st_freeswitch.stat.exists
- find:
    paths: /usr/local/share/freeswitch/scripts
  register: find_freeswitch_scripts
- name: install basic freeswitch scripts
  synchronize:
    src: fs_scripts/
    dest: /usr/local/share/freeswitch/scripts
    owner: no # we want root to own
    group: no
  # only install scripts when empty
  when: "find_freeswitch_scripts|int == 0"
- file:
    state: directory
    path: /etc/monit/conf.d
- name: monitor freeswitch
  template:
    src: freeswitch.j2
    dest: /etc/monit/conf.d/freeswitch
  notify: reload monit
- name: install firewall rules
  template:
    src: rules.v4.j2
    dest: /etc/iptables/rules.v4
  notify: restart netfilter-persistent
- name: install freeswitch_clean
  template:
    src: "{{ item }}.j2"
    dest: "/etc/systemd/system/{{ item }}"
  with_items:
    - freeswitch_clean.timer
    - freeswitch_clean.service
  notify: systemd daemon-reload
- name: enable and start freeswitch_clean.timer
  systemd:
    name: freeswitch_clean.timer
    state: started
    enabled: yes
    daemon_reload: yes
- name: enable and start freeswitch
  systemd:
    name: freeswitch
    state: started
    enabled: yes
    daemon_reload: yes
