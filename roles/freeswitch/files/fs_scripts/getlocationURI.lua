#!/usr/bin/lua

freeswitch.consoleLog("info","Retrieve Location\n")

geolocation     = session:getVariable("sip_geolocation")
id              = session:getVariable("uuid");
BidID           = session:getVariable("sip_h_X-Bid_ID");
locationbyvalue = session:getVariable("sip_multipart");


if (BidID == nil) then
 BidID       = session:getVariable("sip_from_user");
end


if (geolocation == nil) and (locationbyvalue == nil) then
freeswitch.consoleLog("info","No Location Info\n")
return
end






-- fire event to event socket
local event = freeswitch.Event("CUSTOM", "myevent::message");
event:addHeader("My-Name", "LOCATION_DATA");
if (geolocation ~= nil) then
   event:addHeader("Location-URI", geolocation);
   freeswitch.consoleLog("info","LocationUri="..geolocation.."\n")
end
if (locationbyvalue ~= nil) then
   event:addHeader("Location-Data", locationbyvalue);
   freeswitch.consoleLog("info","Location by Value = "..locationbyvalue.."\n")
end

event:addHeader("Channel-Call-UUID", id);
event:addHeader("Bid-ID", BidID);
event:fire();

session:sleep(500);
