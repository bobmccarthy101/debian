#!/usr/bin/lua
freeswitch.consoleLog("INFO","Checking for Conferences v2.1\n")
-- Version 1.1
-- added OriginationCaller variable check.
-- multiple calls from the same number erroneously were
-- tagged as attended Transfers.
-- Version 1.2
-- added transferprefix as argument 1.
-- Version 1.3
-- added destination check
-- Version 1.4
-- Removed Conference check was erroneously finding GUI Blind Transfers ....
-- Version 1.5
-- added network/audiocodes check to 1XX variables
-- Version 1.6
-- added user_exists to catch all users
-- Version 1.7
-- added Bugfix TransferPossible is a string not a boolean
-- Version 1.8
-- added Attended Transfer from Conference is now rejected
-- Version 1.9
-- added Check for 'sip:' prefix in transfer channel
-- Version 2.0
-- added Fixed list vs line in conference check 
-- Version 2.1
-- added check for ',username@' in transfer chaneel


transferprefix = argv[1]


session:execute("set","all_conferences_list=${conference(list)}");
allconferenceslist = session:getVariable("all_conferences_list");

channel  = session:getVariable("channel_name");
destdial = session:getVariable("destination_number");
id       = session:getVariable("uuid");
xfer_uuid= session:getVariable("transfer_uuid");

OriginationCaller = session:getVariable("sip_from_user");
networkaddress = session:getVariable("network_addr");
audiocodesone = session:getVariable("AUDIOCODES-1");
audiocodestwo = session:getVariable("AUDIOCODES-2");
domain        = session:getVariable("domain");
sounddir      = session:getVariable("sounds_dir");


freeswitch.consoleLog("info","Network Address: "..networkaddress.."\n")
freeswitch.consoleLog("info","AC One Address: "..audiocodesone.."\n")
freeswitch.consoleLog("info","AC Two Address: "..audiocodestwo.."\n")

-- check if destination is a 1XX number from audiocodes 1 or 2  (v1.5)
if string.match(networkaddress, audiocodesone) or string.match(networkaddress, audiocodestwo) then
  inboundcall = string.match(destdial, '^1%d%d$');
  if (inboundcall ~= nil) then
   freeswitch.consoleLog("info","No Conference or Transfer: Destination number "..inboundcall.."\n")
   return
  end
end


-- See if the From User is one of our Lines

api = freeswitch.API();

found            = false;

TransferPossible = api:executeString("user_exists id "..OriginationCaller.." "..domain);
freeswitch.consoleLog("INFO","user - "..OriginationCaller.."\n");
freeswitch.consoleLog("INFO","Transfer Possible - "..TransferPossible.."\n");


if (TransferPossible=="true") then
freeswitch.consoleLog("INFO","Transfer Check of Bridged Calls\n");


reply = api:executeString("show bridged_calls");

freeswitch.consoleLog("INFO","Bridged Calls = "..reply.. "\n")

found = false;
--freeswitch.consoleLog("info","Transfer Channel = "..channel.."\n")
 for line in string.gmatch(reply, "(.-)\n") do

        if ((string.find(line ,"nal/"..OriginationCaller.."@") ~= nil) or (string.find(line ,"nal/sip:"..OriginationCaller.."@") ~= nil) 
            or (string.find(line ,","..OriginationCaller.."@")~=nil) or (string.find(line ,",sip:"..OriginationCaller.."@") ~= nil) ) then
        found = true;
        count = 0;
                for test in string.gmatch(line, ",(%w%w%w%w%w%w%w%w%-%w%w%w%w%-%w%w%w%w%-%w%w%w%w%-%w%w%w%w%w%w%w%w%w%w%w%w),") do
                        count = count +1;
                        if (count ==2) then
                        session:setVariable("transfer_uuid", test)
                        end
                end
        end

 end


 if(found) then
 freeswitch.consoleLog("info","Channel found = "..OriginationCaller.."\n")
 end

 if(found) then
 api = freeswitch.API()

  if (transferprefix ~= nil) then
   data = id.." "..destdial.." XML "..transferprefix.."_bridged_att_transfer_context"
  else
   data = id.." "..destdial.." XML bridged_att_transfer_context"
  end

 result = api:execute('uuid_transfer', data);
 end



-- Conference check
 
 session:execute("set","all_conferences_list=${conference(list)}");
 allconferenceslist = session:getVariable("all_conferences_list");
 
 freeswitch.consoleLog("INFO","Conference List Check of User "..OriginationCaller.."\n");

 for conference_uuid in string.gmatch(allconferenceslist, "Conference (%w%w%w%w%w%w%w%w%-%w%w%w%w%-%w%w%w%w%-%w%w%w%w%-%w%w%w%w%w%w%w%w%w%w%w%w) ") do

  data="conference_list=${conference "..conference_uuid.." list}"
  session:execute("set",data);
  list = session:getVariable("conference_list");
   if ((string.find(list ,"nal/"..OriginationCaller.."@") ~= nil) or (string.find(list ,"nal/sip:"..OriginationCaller.."@") ~= nil)) then   
    -- OriginationCaller is in this conference
    freeswitch.consoleLog("info", OriginationCaller.." is in conference "..conference_uuid.."\n")

    session:execute("set","Conference-UUID="..conference_uuid);
			
    -- we cannot do a normal polycom attended transfer from a conference
    -- we will Play an error message and reject the call

    freeswitch.consoleLog("CRIT","Reject AttXfer from Conference "..conference_uuid.." from user "..OriginationCaller.."\n")
    data = "Event-Subclass=myevent::message,Event-Name=CUSTOM,My-Name=ATTXFER_REJ, TransferTo="..destdial..", PositionIP="..networkaddress
    session:execute("event", data);
    session:execute("playback", "ivr/8000/ivr-call_rejected.wav");
    session:execute("hangup", "CALL_REJECTED");

    return
   end 
 end		




end  -- if (TransferPossible)

