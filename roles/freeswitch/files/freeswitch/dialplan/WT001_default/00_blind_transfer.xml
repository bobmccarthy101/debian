<?xml version="1.0" encoding="utf-8"?>
<!--
    This context is designed only to be accessed by channels which are bridged and receive a
    transfer/REFER.  The force_transfer_context channel variable is set prior to executing a
    bridge so that the channel's ROUTING state will process this context for handling a transfer/REFER.

    This context is NOT designed to be associated with any SIP profile.  If that is maintained, no call sent to
    this box can directly access this context (at least that is one way to implement this).
    
    When a channel performs a bridge in another context (e.g. the default context), do your bridge something like this:
    <action application="set" data="force_transfer_context=transfer_handler"/>
    <action application="bridge" data="YOUR DESTINATION!!!"/>
    
    However, BEWARE!!!  Leaving force_transfer_context set to this context will affect ANY transfer of a channel
    for the time that force_transfer_context is set.
    
    So, for example, if you have some kind of continuation after a bridge, you may want to do something like:
    
    <action application="set" data="force_transfer_context=transfer_handler"/>
    <action application="bridge" data="YOUR DESTINATION!!!"/>
    <action application="unset" data="force_transfer_context"/>
--> 
<include>
  <context name="WT001_blind_transfer_handler">

    <!-- transfer_handler extensions explicitly defined in this file go here.  -->
    <!-- Any extensions here will be processed first.                          -->
    <extension name="unloop">
      <condition field="${unroll_loops}" expression="^true$"/>
      <condition field="${sip_looped_call}" expression="^true$">
        <action application="deflect" data="${destination_number}"/>
      </condition>
    </extension>

    <!-- Set ringback and transfer_ringback to provide US ringback.      -->
    <!-- If this freeswitch server receives a 180, US ringback will be   -->
    <!-- locally generated and the caller to this freeswitch server will -->
    <!-- hear it.  (If a 183 is received, the associated media is passed -->
    <!-- to the original caller.)                                        -->
    <extension name="set_default_ringback" continue="true">
      <condition field="destination_number" expression="^.*$" break="never">
        <action application="set" data="ringback=${us-ring}"/>
        <action application="set" data="transfer_ringback=${us-ring}"/>
      </condition>
    </extension>

 


<!-- In this Section                                                            				-->
<!-- 1. If there is only One Gateway(Method) for Outbound Calls:                				-->
<!--    a. Set <condition field="destination_number" expression="^(.*)$">       				-->
<!--       and move block of code to just after these comments                  				-->
<!--      (the others blocks will never get executed)                           				-->
<!--                                                                            				-->
<!-- 2. If there is more than One Gateway(Method) for Outbound Calls:           				-->
<!--    a. Set <regex field="destination_number" expression="^Pattern$" />      				-->
<!--       you will have to set All possible matches                            				-->
<!--    b. Typical Patterns:                                                    				-->
<!--       <condition regex="any">                                              				-->
<!--       <regex field="destination_number" expression="^9(\d{10}$" />           9 with a 10 digit number  	-->
<!--       <regex field="destination_number" expression="^9(\d{4}$" />            9 with a 4 digit number  	-->
<!--       <regex field="destination_number" expression="^9(\d{7}$" />            9 with a 7 digit number  	-->
<!--       see http://wiki.freeswitch.org/wiki/Regular_Expression<regex  					-->
<!--                                                                            				-->

<!--
   <extension name="park-in">   
    <condition regex="any">
      <regex field="destination_number" expression="^(7000)$"/>
      <regex field="destination_number" expression="^park_in$"/>

       <action application="set" data="valet_parking_timeout=90" />
       <action application="set" data="valet_parking_orbit_exten=$${WT001_ADMIN_LINE_ONE}" />
       <action application="set" data="valet_parking_orbit_context=default" />
       <action application="set" data="valet_parking_orbit_dialplan=XML" />
       <action application="valet_park" data="my_lot auto in 7001 7099"/>
    </condition>
   </extension>
-->
   <extension name="BLIND_TRANSFER_AUDIOCODES_LOCAL">
    <condition regex="any">
	    <regex field="destination_number" expression="^(?:1?406)?((232|233|851|852|853|944|951|234|874|934)\d{4})$" />

        <action application="event" data="Event-Subclass=myevent::message,Event-Name=CUSTOM,My-Name=BLIND_XFER, TransferTo=${destination_number}, PositionIP=${network_addr}"/>
        <action application="set" data="hangup_after_bridge=true"/>
	<action application="set" data="continue_on_fail=true"/>
        <action application="export" data="nolocal:absolute_codec_string=PCMU,PCMA"/>    
        <action application="bridge" data="[bridge_generate_comfort_noise=true]sofia/internal/$1@$${audiocodes-1}"/>
        <action application="bridge" data="[bridge_generate_comfort_noise=true]sofia/internal/$1@$${audiocodes-2}"/>
        <action application="event" data="Event-Subclass=myevent::message,Event-Name=CUSTOM,My-Name=RING_BACK, TransferTo=${destination_number}, PositionIP=${network_addr}"/>
        <action application="sleep" data="500"/>
        <action application="lua" data="hanguphook.lua"/>
        <action application="bridge" data="[origination_callee_id_name=${user_data(${dialed_user}@${domain} var effective_caller_id_name},
                                            origination_callee_id_number=${user_data(${dialed_user}@${domain} var effective_caller_id_number}]user/${dialed_user}@$${domain_name}"/>
        <action application="execute_extension" data="${originate_disposition} XML features"/>

        <!-- RingBack ?? -->
      </condition>
    </extension>
   <extension name="BLIND_TRANSFER_AUDIOCODES_NONLOCAL_D7_1406">
    <condition regex="any">
	    <regex field="destination_number" expression="^(\d{7})$" />

        <action application="event" data="Event-Subclass=myevent::message,Event-Name=CUSTOM,My-Name=BLIND_XFER, TransferTo=${destination_number}, PositionIP=${network_addr}"/>
        <action application="set" data="hangup_after_bridge=true"/>
	<action application="set" data="continue_on_fail=true"/>
        <action application="export" data="nolocal:absolute_codec_string=PCMU,PCMA"/>    
        <action application="bridge" data="[bridge_generate_comfort_noise=true]sofia/internal/1406$1@$${audiocodes-1}"/>
        <action application="bridge" data="[bridge_generate_comfort_noise=true]sofia/internal/1406$1@$${audiocodes-2}"/>
        <action application="event" data="Event-Subclass=myevent::message,Event-Name=CUSTOM,My-Name=RING_BACK, TransferTo=${destination_number}, PositionIP=${network_addr}"/>
        <action application="sleep" data="500"/>
        <action application="lua" data="hanguphook.lua"/>
        <action application="bridge" data="[origination_callee_id_name=${user_data(${dialed_user}@${domain} var effective_caller_id_name},
                                            origination_callee_id_number=${user_data(${dialed_user}@${domain} var effective_caller_id_number}]user/${dialed_user}@$${domain_name}"/>
        <action application="execute_extension" data="${originate_disposition} XML features"/>

        <!-- RingBack ?? -->
      </condition>
    </extension>
   <extension name="BLIND_TRANSFER_AUDIOCODES_LONG_DISTANCE">
    <condition regex="any">
	    <regex field="destination_number" expression="^1?(\d{10})$" />

        <action application="event" data="Event-Subclass=myevent::message,Event-Name=CUSTOM,My-Name=BLIND_XFER, TransferTo=${destination_number}, PositionIP=${network_addr}"/>
        <action application="set" data="hangup_after_bridge=true"/>
	<action application="set" data="continue_on_fail=true"/>
        <action application="export" data="nolocal:absolute_codec_string=PCMU,PCMA"/>    
        <action application="bridge" data="[bridge_generate_comfort_noise=true]sofia/internal/1$1@$${audiocodes-1}"/>
        <action application="bridge" data="[bridge_generate_comfort_noise=true]sofia/internal/1$1@$${audiocodes-2}"/>
        <action application="event" data="Event-Subclass=myevent::message,Event-Name=CUSTOM,My-Name=RING_BACK, TransferTo=${destination_number}, PositionIP=${network_addr}"/>
        <action application="sleep" data="500"/>
        <action application="lua" data="hanguphook.lua"/>
        <action application="bridge" data="[origination_callee_id_name=${user_data(${dialed_user}@${domain} var effective_caller_id_name},
                                            origination_callee_id_number=${user_data(${dialed_user}@${domain} var effective_caller_id_number}]user/${dialed_user}@$${domain_name}"/>
        <action application="execute_extension" data="${originate_disposition} XML features"/>

        <!-- RingBack ?? -->
      </condition>
    </extension>
   <extension name="BLIND_TRANSFER_AUDIOCODES">
      <condition field="destination_number" expression="^(.*)$">

        <action application="event" data="Event-Subclass=myevent::message,Event-Name=CUSTOM,My-Name=BLIND_XFER, TransferTo=${destination_number}, PositionIP=${network_addr}"/>
        <action application="set" data="hangup_after_bridge=true"/>
	<action application="set" data="continue_on_fail=true"/>
        <action application="export" data="nolocal:absolute_codec_string=PCMU,PCMA"/>    
        <action application="bridge" data="[bridge_generate_comfort_noise=true]sofia/internal/${destination_number}@$${audiocodes-1}"/>
        <action application="bridge" data="[bridge_generate_comfort_noise=true]sofia/internal/${destination_number}@$${audiocodes-2}"/>
        <action application="event" data="Event-Subclass=myevent::message,Event-Name=CUSTOM,My-Name=RING_BACK, TransferTo=${destination_number}, PositionIP=${network_addr}"/>
        <action application="sleep" data="500"/>
        <action application="lua" data="hanguphook.lua"/>
        <action application="bridge" data="[origination_callee_id_name=${user_data(${dialed_user}@${domain} var effective_caller_id_name},
                                            origination_callee_id_number=${user_data(${dialed_user}@${domain} var effective_caller_id_number}]user/${dialed_user}@$${domain_name}"/>
        <action application="execute_extension" data="${originate_disposition} XML features"/>

        <!-- RingBack ?? -->
      </condition>
    </extension>


   <extension name="BLIND_TRANSFER_INVALID_NUMBER">
      <condition field="destination_number" expression="^(.*)$">

        <action application="transfer" data="INVALID_NUMBER_FORMAT XML features"/>
      </condition>
    </extension>



  </context>
</include>  




 

