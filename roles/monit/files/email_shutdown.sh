#!/bin/bash

MAIL=(mail alarm@westtel.com)
TO_ADDR=psap@westtel.com

if [ "$1" == "start" ]; then
	"${MAIL[@]}" -s "[ALARM] `hostname` Starting Up" "$TO_ADDR" <<HERE_STRING
PSAP       = $PSAP_NAME
Time Stamp = `date '+%F %H:%M:%S %Z'` | `date -u '+%F %H:%M:%S %Z'`
Server     = `hostname`
Message    = The 9-1-1 server is starting up
HERE_STRING
	sleep 1
elif [ "$1" == "stop" ]; then
	"${MAIL[@]}" -s "[ALARM] `hostname` Shutting Down" "$TO_ADDR" <<HERE_STRING
PSAP       = $PSAP_NAME
Time Stamp = `date '+%F %H:%M:%S %Z'` | `date -u '+%F %H:%M:%S %Z'`
Server     = `hostname`
Message    = The 9-1-1 server is shutting down!
HERE_STRING
	sleep 5
fi

